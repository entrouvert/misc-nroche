La procédure de migration est la suivante :


Récupérer le mot de passe w.c.s. de connexion à la base de donnée
-----------------------------------------------------------------

Il faut récupérer le mot de passe sur une autre instance, sur la cible :
https://demarches-eurelien.test.entrouvert.org/backoffice/settings/postgresql
puis utiliser l'inspecteur pour récupérer le mot de passe.
Ou sinon en ligne de commande :
::

   $ sudo -u wcs wcs-manage shell -d demarches-eurelien.test.entrouvert.org
   In [1]: from quixote import get_publisher
   In [2]: print(get_publisher().cfg['postgresql'].get('createdb-connection-params', {}))


Récupérer les identifiant de connexion à la BD et le dsn sentry sur wcs-olap
----------------------------------------------------------------------------

Il faut récupérer ces information sur la cible, dans les fichiers
wcs-olap.ini des instances déjà migrées (en recette, il s'agit du même
mot de passe que pour w.c.s).


Recopier le certificat
----------------------

Pour la recette rien a faire le certificat wildcard est déja en place
pour test.entrouvert.org
::

   $ haproxy.test-hds.saas.entrouvert:/etc/ssl/bundles$ openssl x509 -in test.entrouvert.org.pem -text


Re-diriger vers une page de travaux
-----------------------------------

Ce n'est pas encore possible en recette.


Jouer une première fois le scripts pour cibler le bon noeud
-----------------------------------------------------------

(Le script doit être lancé sur le noeud où les crons ne sont pas désactivés)
::

   $ publik_dump/publik_dump.py tenantinfo node2.test.saas.entrouvert.org hobo-ac-versailles.test.entrouvert.org --update

Si un tenant est trop lourd (w.c.s à 18G)
-----------------------------------------

* Faire la synchro via rsync

Le tar prend beaucoup de temps et donc autant l'éviter.
Faire le rsync en 2 temps depuis son poste pour ne pas avoir à poser de nouvelle clé ssh entr les 2 SaaS.
Recopier le répertoire cibble en .invalid
Puis penser à repositionner les droits sur le répertoire wcs.

* Lors de la migration, sauter le dump et le restore w.c.s.
  en décommentant ces 4 lignes dans le script :

::

   #if service["name"] == "wcs":
   #    continue  # in order to run rsync instead, a second time on large tenant dump

* Puis synchroniser le répertoire via rsync


Invalider les tenants
---------------------

Pour ne pas avoir les cron qui se jouent sur les 2 instances.
::

   $ publik_dump/publik_dump.py invalidate_source node2.test.saas.entrouvert.org hobo-ac-versailles.test.entrouvert.org


Dump
----

(tenant + base de donnée)
::

   $ publik_dump/publik_dump.py dump node2.test.saas.entrouvert.org hobo-ac-versailles.test.entrouvert.org

Restore
-------
::

   $ publik_dump/publik_dump.py restore node2.test.saas.entrouvert.org hobo-ac-versailles.test.entrouvert.org --target node1.test-hds.saas.entrouvert --dbtarget sql3.test-hds.saas.entrouvert


Mise à jour des identifiant de connexion à la base de donnée de w.c.s.
----------------------------------------------------------------------

Modification du pickle config.pck :
::

   $ scp publik_dump/adapt_wcs_config.py wcs.node1.test-hds.saas.entrouvert:.
   $ ssh wcs.node1.test-hds.saas.entrouvert
   $ sudo -u wcs ./adapt_wcs_config.py /var/lib/wcs/demarches-ac-versailles.test.entrouvert.org.invalid/config.pck --host test-hds.saas.entrouvert.clusters.entrouvert.org --password XXX

Remarque, ici on peut tester la connexion :
::

   $ ssh wcs.node1.test-hds.saas.entrouvert
   $ psql -U wcs -h test-hds.saas.entrouvert.clusters.entrouvert.org
   Mot de passe pour l'utilisateur wcs : XXX

et aussi que tout va bien côté wcs :
::

   $ sudo -u wcs wcs-manage shell -d demarches-ac-versailles.test.entrouvert.org.invalid

Mise à jour des identifiant de connexion à la BD et du dsn sentry de wcs-olap
-----------------------------------------------------------------------------

Modification de wcs-olap.ini :
::

   $ ssh bijoe.node1.test-hds.saas.entrouvert
   $ sudo -u bijoe vi /var/lib/bijoe/tenants/statistiques-ac-versailles.test.entrouvert.org.invalid/wcs-olap.ini
   ...
   pg_dsn = dbname='bijoe' host='test-hds.saas.entrouvert.clusters.entrouvert.org' password='YYY'

   [sentry]
   dsn = https://39e69f265d6d41adbba9cbdd9f1d4573@sentry.entrouvert.org/6
   environment = test
   ...

Remarque, ici on peut tester la connexion et l'import :
::

   ssh bijoe.node1.test-hds.saas.entrouvert
   psql -U bijoe -h test-hds.saas.entrouvert.clusters.entrouvert.org
   Mot de passe pour l'utilisateur bijoe : YYY
   bijoe=> set search_path="statistiques_ac_versailles_test_entrouvert_org"
   bijoe=> \d
   bijoe=> select * from auth_user;


Valider les tenants cibles
--------------------------

Cela va également rétablir les crons.
::

   $ publik_dump/publik_dump.py validate_target node2.test.saas.entrouvert.org hobo-ac-versailles.test.entrouvert.org --target node1.test-hds.saas.entrouvert


Rejouer les migrations Passerelle
---------------------------------

Parce qu'il y a les deux connecteurs mdph13 et rsa13 en plus sur
l'HDS, qui ne sont pas déployé sur le SaaS.
::

   $ ssh passerelle.node1.test-hds.saas.entrouvert
   $ sudo -u passerelle passerelle-manage migrate_schemas -v2


Jouer wcs-olap
--------------

Bijoe est particulier, il n'y a pas juste le schéma du tenant bijoe, il y a aussi les schémas des w.c.s. moisonnés.
Le scrip ne prend pas encore ça en charge.
Il faut relancer wcs-olap à la main (à présent lancé via cron uwsgi) :
::

   $ ssh bijoe.node1.hds.saas.entrouvert
   $ sudo -u bijoe wcs-olap --all /var/lib/bijoe/tenants/statistiques.demarches.ac-versailles.fr/wcs-olap.ini

On peut vérifier que le schéma demarches.demarches.ac-versailles.fr existe et est rempli :
::

   $ ssh bijoe.node1.hds.saas.entrouvert
   $ psql -U bijoe -h hds.saas.entrouvert.clusters.entrouvert.org
   $ \dn
                     Liste des schémas
                     Nom                     | Propriétaire
   --------------------------------------------+--------------
   demarches_demarches_ac_versailles_fr_temp  | bijoe


Modifier les DNS
----------------


Retirer la redirection haproxy du SaaS
--------------------------------------

Ou pas.


Penser à supprimer les cookies versailles
-----------------------------------------

Ou à tester depuis une fenêtre de navigation privée, sinon la session du navigateur est corrompue
::

   $ ssh combo.node1.test-hds.saas.entrouvert
   $ sudo journalctl TENANT="portail-ac-versailles.test.entrouvert.org"
   mai 02 10:36:42 combo uwsgi[2249254]: combo WARNING portail-ac-versailles.test.entrouvert.org - - - Session data corrupted

Aussi parfois il faut passer par le VPN pour avoir la mire de connexion.
