#!/usr/bin/python3
import json
from urllib.parse import urlsplit
from hobo.environment.models import AVAILABLE_SERVICES
from hobo.multitenant.middleware import TenantMiddleware
from django.db import connection

tenant = connection.get_tenant()
services = []

for service in AVAILABLE_SERVICES:
    for site in service.objects.all():
        service = {
            "name": site.Extra.service_id,
            "url": urlsplit(site.base_url).netloc.split(":")[0],
        }
        if site.secondary:
            service["secondary"] = True
        schema = TenantMiddleware.hostname2schema(service["url"])
        if service["name"] == "wcs":
            schema = "wcs_%s" % schema
        service["schema"] = schema
        services.append(service)

print(json.dumps({"name": tenant.domain_url, "services": services}))
