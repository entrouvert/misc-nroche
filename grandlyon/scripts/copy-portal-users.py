#! /usr/bin/env python
# python3 copy-portal-users.py Oullins Genay

import json
import os
import subprocess
import sys

from django.utils.text import slugify

orig_city= sys.argv[1]
slug_orig_city = slugify(orig_city)
dest_city = sys.argv[2]
slug_dest_city = slugify(dest_city)

if os.path.exists('/var/lib/combo/tenants/www.toodego.com'):
    orig_site = '%s.toodego.com' % slug_orig_city
    dest_site = '%s.toodego.com' % slug_dest_city
else:
    orig_site = 'portail-citoyen-%s.guichet-recette.grandlyon.com' % slug_orig_city
    dest_site = 'portail-citoyen-%s.guichet-recette.grandlyon.com' % slug_dest_city

site_export_orig = subprocess.check_output(['sudo', '-u', 'combo', 'combo-manage', 'tenant_command', 'export_site', '-d', orig_site, '--format-json'])
site_export_orig = site_export_orig.decode('utf-8')

site_export = site_export_orig.replace(orig_city, dest_city)
#site_export = site_export.replace('Pierre-B&eacute;nite', dest_city)
#site_export = site_export.replace('Pierre-B\\u00e9nite', dest_city)
site_export = site_export.replace(slug_orig_city, slug_dest_city)
open('/tmp/site-export.json', 'w').write(site_export)
subprocess.call(['sudo', '-u', 'combo', 'combo-manage', 'tenant_command', 'import_site', '-d', dest_site, '/tmp/site-export.json'])
subprocess.call(['sudo', '-u', 'combo', 'combo-manage', 'tenant_command', 'runscript', '-d', dest_site, 'fsck-combo.py'])

orig_media_dir = '/var/lib/combo/tenants/%s/media' % orig_site
if os.path.exists(orig_media_dir) and os.listdir(orig_media_dir):
    subprocess.call(['sudo', '-u', 'combo', 'cp', '-ar', orig_media_dir, '/var/lib/combo/tenants/%s/' % dest_site])
