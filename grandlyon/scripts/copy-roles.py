import os
import json
import sys

from authentic2.a2_rbac.models import OrganizationalUnit, Role

mapping = {}

name_src = sys.argv[1]
name_dst = sys.argv[2]

ou_src = OrganizationalUnit.objects.get(name=name_src)
ou = OrganizationalUnit.objects.get(name=name_dst)

agent_role = None

for role in Role.objects.filter(ou=ou_src):
    #if not (role.name.startswith('Gestionnaire') or role.name.startswith('Administrateur')):
    #    continue
    if role.slug.startswith('_'):
        continue
    new_name = role.name.replace(name_src, name_dst).replace(name_src.lower(), name_dst.lower())
    kwargs = {'ou': ou, 'name': new_name}
    if name_dst.lower() not in new_name.lower():
        kwargs['slug'] = role.slug
    new_role, created = Role.objects.get_or_create(**kwargs)
    mapping[role.uuid] = {name_dst: new_role.uuid}
    if new_name == 'Agent':
        agent_role = new_role

if agent_role:
    for role in Role.objects.filter(ou=ou):
        if ou.uuid == agent_role.uuid:
            continue
        role.add_parent(agent_role)

json.dump(mapping, open('/tmp/roles.mapping.json', 'w'), indent=2)
