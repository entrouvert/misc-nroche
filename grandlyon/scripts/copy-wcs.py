#! /usr/bin/env python
# usage: sudo -u wcs /usr/bin/wcsctl -f /etc/wcs/wcs-au-quotidien.cfg runscript --vhost=demarches-<...>.guichet-recette.grandlyon.com copy-wcs.py Oullins Genay

from quixote import get_publisher
from wcs.carddef import CardDef
from wcs.formdef import FormDef

import json
import os
import subprocess
import sys

from django.utils.text import slugify

orig_city = sys.argv[1]
slug_orig_city = slugify(orig_city)
dest_city = sys.argv[2]
slug_dest_city = slugify(dest_city)

if os.path.exists('/var/lib/combo/tenants/www.toodego.com'):
    orig_site = 'demarches-%s.toodego.com' % slug_orig_city
    dest_site = 'demarches-%s.toodego.com' % slug_dest_city
else:
    orig_site = 'demarches-%s.guichet-recette.grandlyon.com' % slug_orig_city
    dest_site = 'demarches-%s.guichet-recette.grandlyon.com' % slug_dest_city

roles_mapping = json.load(open('/tmp/roles.mapping.json'))

def copy(orig, dest, dest_city):
    if os.path.exists(dest):
        return  # do not overwrite
    if not os.path.isfile(orig):
        return  # ignore non files
    src = open(orig, 'rb').read()
    for role_id in roles_mapping.keys():
        src = src.replace(role_id.encode('ascii'), roles_mapping[role_id][dest_city].encode('ascii'))
    fd = open(dest, 'wb')
    fd.write(src)
    fd.close()

# cf ~/src/wcs/wcs/publisher.py::import_zip
# $ git grep -A1 XmlStorableObject wcs | grep '_names ='
# models: les modèles de documents
# puis faire un ls sur le tenant de la souce pour voir les répertoires à copier
# 'apiaccess', 'blockdefs'  # jamais testé
for object_type in ('carddefs', 'formdefs', 'workflows', 'categories', 'datasources', 'models', 'wscalls',
                    'carddef_categories', 'workflow_categories', 'block_categories', 'mail_template_categories',
                    'data_source_categories', 'comment_template_categories',
                    'mail-templates', 'comment-templates'):


    if os.path.exists('/var/lib/wcs/%s' % orig_site):
        orig_base_dir = '/var/lib/wcs/%s' % orig_site
    elif os.path.exists('/var/lib/wcs/tenants/%s' % orig_site):
        orig_base_dir = '/var/lib/wcs/tenants/%s' % orig_site
    else:
        print('missing source tenant')
        sys.exit(1)
    dest_base_dir = get_publisher().app_dir
    orig_dir = os.path.join('%s/%s/' % (orig_base_dir, object_type))
    dest_dir = os.path.join('%s/%s/' % (dest_base_dir, object_type))
    if not os.path.exists(orig_dir):
        continue
    if not os.path.exists(dest_dir):
        os.mkdir(dest_dir)
    for object_id in os.listdir(orig_dir):
        copy(os.path.join(orig_dir, object_id),
             os.path.join(dest_dir, object_id),
             dest_city)

# iterate over all objects and store them again to make sure side-code is run.
for formdef in FormDef.select():
    if orig_city in formdef.name:
        formdef.name = formdef.name.replace(orig_city, dest_city)
    formdef.store()
    formdef.update_storage()

for carddef in CardDef.select():
    carddef.update_storage()
