# run .save() to fill cached properties

from combo.apps.wcs.models import WcsFormCell, WcsCategoryCell, WcsFormsOfCategoryCell

for obj_type in (WcsFormCell, WcsCategoryCell, WcsFormsOfCategoryCell):
    for obj in obj_type.objects.all():
        obj.save()
