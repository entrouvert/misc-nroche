# add all collectivity technical admin roles to "Administrateur <commune>"

# authentic2-multitenant-manage tenant_command runscript -d connexion.guichet-recette.grandlyon.com ~tma/misc-fred/grandlyon/scripts/mark-admin-roles.py Genais

import os
import json
import sys

from django.db.models.query import Q

from authentic2.a2_rbac.models import OrganizationalUnit, Role


name_dst = sys.argv[1]
ou = OrganizationalUnit.objects.get(name=name_dst)

role = Role.objects.get(name=u'Administrateur %s' % name_dst, ou=ou)
for subrole in Role.objects.filter(Q(slug='_a2-hobo-superuser', ou=ou) |
                Q(slug__startswith='administrateur-fonc', ou=ou) |
                Q(slug__startswith='_a2-administrateur', ou=ou)):
    role.add_parent(subrole)
