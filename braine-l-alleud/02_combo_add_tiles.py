#!/usr/bin/env python
# ex:
# $ ssh combo.node2.test.saas.entrouvert.org
# $ sudo -u combo combo-manage shell -d portail-braine-l-alleud.test.entrouvert.org < 02_combo_add_tiles.py

import json

from combo.data.models import ConfigJsonCell, Page


settings = {
    'tiles title page': 'Mon compte',
    'bla_con_slug': 'site-web',
}

page = Page.objects.get(title=settings['tiles title page'])
order = len(page.get_cells())
nb_created = 0

# add deliberation cell
kwargs = {
    'placeholder': 'content',
    'key': 'deliberations-communales'
}
try:
    ConfigJsonCell.objects.get(**kwargs)
except ConfigJsonCell.DoesNotExist:
    print(kwargs)
    kwargs.update({
        'page': page,
        'order': order + nb_created,
    })
    ConfigJsonCell.objects.create(**kwargs).save()
    nb_created += 1

# add bla cells
con_slug = settings['bla_con_slug']
for query_slug in 'avis_et_enquete', 'info_travaux':
    kwargs = {
        'placeholder': 'content',
        'parameters__connector': con_slug,
        'parameters__query': query_slug,
    }
    try:
        ConfigJsonCell.objects.get(**kwargs)
    except ConfigJsonCell.DoesNotExist:
        print(kwargs)
        kwargs = {
            'placeholder': 'content',
            'parameters': {'connector': con_slug, 'query': query_slug},
            'page': page,
            'key': 'actualites',
            'order': order + nb_created,
        }
        ConfigJsonCell.objects.create(**kwargs).save()
        nb_created += 1

# add category_and_topics related cells
with open('topics.json') as fd:
    settings = json.load(fd)
for con_slug in settings.keys():
    field_values = sorted(settings[con_slug]['field_values'], key=lambda x: x['title'])
    for field_value in field_values:
        query_slug = field_value['token']
        kwargs = {
            'placeholder': 'content',
            'parameters__connector': con_slug,
            'parameters__query': query_slug,
        }
        try:
            ConfigJsonCell.objects.get(**kwargs)
        except ConfigJsonCell.DoesNotExist:
            kwargs = {
                'placeholder': 'content',
                'parameters': {'connector': con_slug, 'query': query_slug},
                'page': page,
                'key': settings[con_slug]['cell_key'],
                'order': order + nb_created,
            }
            print(kwargs)
            ConfigJsonCell.objects.create(**kwargs).save()
            nb_created += 1
        if nb_created >= 5:
            exit()

if nb_created > 0:
    print('Please reorder the %s added cells on %s page' % (nb_created, page.get_online_url()))
