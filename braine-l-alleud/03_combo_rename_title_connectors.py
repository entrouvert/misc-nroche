#!/usr/bin/env python
# ex:
# $ ssh combo.node2.test.saas.entrouvert.org
# $ sudo -u combo combo-manage shell -d portail-braine-l-alleud.test.entrouvert.org < 03_combo_rename_title_connectors.py

import json

from combo.apps.dashboard.models import Tile
from combo.data.models import ConfigJsonCell, Page

page = Page.objects.get(title='Mon compte')
cons = [
    {'old': 'bla-dev', 'new': 'site-web'},
    {'old': 'news-dev', 'new': 'actualites'},
    {'old': 'events-dev', 'new': 'evenements'},
]

nb_updated_cells = 0
nb_updated_tiles = 0

for con in cons:
    # cells
    kwargs = {
        'page': page,
        'placeholder': 'content',
        'parameters__connector': con['old'],
    }
    for cell in ConfigJsonCell.objects.filter(**kwargs):
        cell.parameters['connector'] = con['new']
        cell.save()
        nb_updated_cells += 1

    # tiles
    for tile in Tile.objects.all():
        if 'connector' in  tile.cell.parameters and tile.cell.parameters['connector'] == con['old']:
            tile.cell.parameters['connector'] = con['new']
            tile.cell.save()
            nb_updated_tiles += 1


print('%s cells updated on %s page' % (nb_updated_cells, page.get_online_url()))
print('%s tiles updated' % nb_updated_tiles)
