#!/usr/bin/env python

# create topics.json having vocabulary for category_and_topics field
# ex:
# $ ssh passerelle.node2.test.saas.entrouvert.org
# $ touch topics.json
# $ chmod 777 topics.json
# $ sudo -u passerelle passerelle-manage shell -d passerelle-braine-l-alleud.test.entrouvert.org < 00_passerelle_list_topics.py

import json

from passerelle.apps.plone_restapi.models import PloneRestApi


# connectors to Plone types having a category_and_topics index
settings = {
    'actualites': {
        'vocabulary_id': 'imio.news.vocabulary.NewsCategoriesAndTopicsVocabulary',
        'plone_type': 'imio.news.NewsItem',
        'uri': 'belleville',
        'description': "Actualités %s",
        'cell_key': 'actualites',
    },
    'evenements': {
        'vocabulary_id': 'imio.events.vocabulary.EventsCategoriesAndTopicsVocabulary',
        'plone_type': 'imio.events.Event',
        'uri': 'bruxelles/administration-communale-de-bruxelles',
        'description': "Événements %s",
        'cell_key': 'evenements',
    },
}

# get values for category_and_topics fields
for key, value in settings.items():
    vocabulary_id = value.pop('vocabulary_id')
    con =  PloneRestApi.objects.get(slug=key)
    resp = con.request(path='@vocabularies/%s' % vocabulary_id, params = {'b_size': 100})
    value['field_values'] = resp['items']

with open('topics.json', 'w') as fd:
    json.dump(settings, fd, indent=2)
