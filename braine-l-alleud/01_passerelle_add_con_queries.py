#!/usr/bin/env python

# ex:
# $ ssh passerelle.node2.test.saas.entrouvert.org
# $ sudo -u passerelle passerelle-manage shell -d passerelle-braine-l-alleud.test.entrouvert.org < 01_passerelle_add_con_queries.py

import json

from passerelle.apps.plone_restapi.models import PloneRestApi


# connector query parameters to add
settings = {
    'deliberations': {
        'deliberations': {
            'name': 'Délibérations',
            'description': "Soyez informés dès que les décisions publiques du Conseil communal auront été approuvées par ses membres.",
            'uri': 'belle-ville',
            'filter_expression': 'portal_type=Meeting',
            'sort': 'date_time',
        },
    },
    'site-web': {
        'avis_et_enquete': {
            'name': "Avis d'urbanisme",
            'description': "Avis d'urbanisme",
            'uri': 'ma-commune/services-communaux/urbanisme-et-amenagement-du-territoire/avis-et-enquetes/avis-2021',
            'filter_expression': 'portal_type=imio.smartweb.Page\r\nreview_state=published\r\ntaxonomy_page_category=avis_et_enquete\r\ntopics=habitat_town_planning',
            'sort': 'effective',
        },
        'info_travaux': {
            'name': 'Infos travaux',
            'description': 'Informations des travaux',
            'uri': 'ma-commune/services-communaux/travaux-et-patrimoine/info-travaux',
            'filter_expression': 'portal_type=imio.smartweb.Page\r\nreview_state=published\r\ntaxonomy_page_category=info_travaux',
            'sort': 'effective',
        },
    },
}

# add topics to settings
with open('topics.json') as fd:
    topics = json.load(fd)
for con_slug, topic in topics.items():
    if not settings.get(con_slug):
        settings[con_slug] = {}
    for field_value in topic['field_values']:
        query_slug = field_value['token']
        settings[con_slug][query_slug] = {
            'con_slug': con_slug,
            'name': field_value['title'],
            'description': topic['description'] % field_value['title'].lower(),
            'uri': topic['uri'],
            'filter_expression': 'portal_type=%s\r\nreview_state=published\r\ncategory_and_topics=%s' % (
                topic['plone_type'],
                query_slug,
            ),
            'sort': 'created',
        }

# update or create connector queries
for con_slug in settings.keys():
    for query_slug, setting in settings[con_slug].items():
        print("update %s/%s" % (con_slug, query_slug))
        con = PloneRestApi.objects.get(slug=con_slug)
        con.queries.update_or_create(slug=query_slug, defaults={
            'name': setting['name'],
            'description': setting['description'],
            'uri': setting['uri'],
            'text_template': '{{ title }}',
            'filter_expression': setting['filter_expression'],
            'sort': setting['sort'],
            'order': False,
            'limit': 5
        })
