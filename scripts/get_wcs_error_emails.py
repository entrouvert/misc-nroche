# $ sudo -u wcs wcs-manage runscript --all-tenants t.py

from quixote import get_publisher
from wcs.qommon import get_cfg
print('%s: "%s"' % (
    get_publisher().tenant.hostname,
    get_cfg('debug', {}).get('error_email')
))
